<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ShippingAddress extends Model
{
    use HasFactory;

    protected $fillable = [
        'ship_street',
        'ship_city',
        'ship_state',
        'ship_zip_code'
    ];

    public function user () {
        return $this->belongsTo(User::class, 'user_id');
    }
}
