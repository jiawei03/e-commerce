<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\ShippingAddress;
use Illuminate\Validation\Rule;

class UserController extends Controller
{
    public function index(Request $request) {
        $authUser = $request->user();
        $user= User::with('shippingAddresses')->find($authUser->id);
        return response()->json(['users' => $user]);
    }
    
    public function register(Request $request) {
        $input = $request->validate([
            'name' => ['required', 'min:3', 'max:20', Rule::unique('users', 'name')],
            'email' => ['required', 'email', Rule::unique('users', 'email')],
            'password' => ['required', 'min:8', 'max:20'], 
            'ship_street' => ['required'],
            'ship_city' => ['required'],
            'ship_state' => ['required'],
            'ship_zip_code' => ['required'],
            'phone' => ['nullable', 'min:10', 'max:11'],
            'bio' => ['nullable'],
            'weight' => ['nullable', 'numeric'],
            'height' => ['nullable', 'numeric'],
            'gender' => ['nullable', Rule::in(['M', 'F'])]
        ]);

        $input['password'] = bcrypt($input['password']);
        $currentUser = User::create($input);

        $shippingAddress = new ShippingAddress([
            'ship_street' => $input['ship_street'],
            'ship_city' => $input['ship_city'],
            'ship_state' => $input['ship_state'],
            'ship_zip_code' => $input['ship_zip_code']
        ]);
    
        $currentUser->shippingAddresses()->save($shippingAddress);
        
        auth()->login($currentUser);

        // return redirect('/');
        return response()->json(['message' => 'User registered successfully', 'user' => $currentUser]);
    }

    public function logout() {
        auth()->logout();
        // return redirect('/');
        return response()->json(['message' => 'User logged out']);
    }

    public function login(Request $request) {
        $input = $request->validate([
            'loginName' => 'required',
            'loginPass' => 'required'
        ]);

        if(auth()->attempt(['name' => $input['loginName'], 'password' => $input['loginPass']])) {
            $request->session()->regenerate();
            return response()->json(['message' => 'Login successful', 'user' => auth()->user()]);
        }

        // return redirect('/');
        return response()->json(['message' => 'Invalid credentials'], 401);
    }

    public function showEditProfileForm() {
        return view('edit-profile');
    }

    public function update(Request $request) {
        $user = auth()->user();
    
        $input = $request->validate([
            'name' => ['nullable', 'min:3', 'max:20', Rule::unique('users', 'name')->ignore($user->id)],
            'email' => ['nullable', 'email', Rule::unique('users', 'email')->ignore($user->id)],
            'password' => ['nullable', 'min:8', 'max:20'], 
            'ship_street' => ['nullable'],
            'ship_city' => ['nullable'],
            'ship_state' => ['nullable'],
            'ship_zip_code' => ['nullable'],
            'phone' => ['nullable', 'min:10', 'max:11'],
            'bio' => ['nullable'],
            'weight' => ['nullable', 'numeric'],
            'height' => ['nullable', 'numeric'],
            'gender' => ['nullable', Rule::in(['M', 'F'])]
        ]);
    
        if (isset($input['password'])) {
            $input['password'] = bcrypt($input['password']);
        } else {
            unset($input['password']);
        }
    
        $user->update($input);
    
        if ($user->shippingAddresses()->exists()) {
            $shippingAddress = $user->shippingAddresses()->first();
            $shippingAddress->update([
                'ship_street' => $input['ship_street'],
                'ship_city' => $input['ship_city'],
                'ship_state' => $input['ship_state'],
                'ship_zip_code' => $input['ship_zip_code']
            ]);
        } else {
            $shippingAddress = new ShippingAddress([
                'ship_street' => $input['ship_street'],
                'ship_city' => $input['ship_city'],
                'ship_state' => $input['ship_state'],
                'ship_zip_code' => $input['ship_zip_code']
            ]);
            $user->shippingAddresses()->save($shippingAddress);
        }
    
        return response()->json(['message' => 'User details updated successfully', 'user' => $user]);
    }
}
