<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;

class ProductController extends Controller
{
    public function index()
    {
        $products = Product::all();
        // return view('welcome', ['products' => $products]);
        return response()->json(['products' => $products]);
    }

    public function productShow(Product $product)
    {
        return response()->json(['product' => $product]);
    }

    public function productCategory($category)
    {
        $products = Product::where('description', $category)->get();
        return response()->json(['products' => $products]);
    }
    
    public function search(Request $request)
    {
        $searchTerm = $request->input('search');

        $products = Product::where(function ($query) use ($searchTerm) {
            $query->where('title', 'like', '%' . $searchTerm . '%')
                ->orWhere('description', 'like', '%' . $searchTerm . '%');
        })->get();

        return response()->json(['products' => $products]);
    }
}
