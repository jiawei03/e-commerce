<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Order;
use Auth;

class OrderController extends Controller
{
    public function index()
    {
        $user = Auth::user();

        $orders = Order::where('user_id', $user->id)->get();

        // return view('orderhistory', ['orders' => $orders]);
        return response()->json(['orders' => $orders]);
    }
}
