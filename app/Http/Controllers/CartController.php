<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Models\Product;
use App\Models\CartItem;
use App\Models\Order;
use Auth;

class CartController extends Controller
{
    public function index(Request $request)
    {
        $user = Auth::user();

        $cartItems = CartItem::where('user_id', $user->id)
                             ->with('product')
                             ->get();

        $totalPrice = $cartItems->sum(function ($cartItem) {
            return $cartItem->quantity * $cartItem->product->price; 
        });

        $data = $cartItems->map(function ($cartItem) {
            return [
                'product' => $cartItem->product,
                'quantity' => $cartItem->quantity,
            ];
        });

        // return response()->json([
        //    'cartItems' => $cartItems,
        //    'totalPrice' => $totalPrice,
        // ]);        
        return view('cart', [
            'cartItems' => $cartItems,
            'totalPrice' => $totalPrice,
        ]);    
    }

    public function addToCart(Request $request, Product $product)
    {
        if (Auth::check()) {
            $userId = Auth::id();

            $existingCartItem = CartItem::where('user_id', $userId)
                                        ->where('product_id', $product->id)
                                        ->first();

            if ($existingCartItem) {
                $existingCartItem->quantity++;
                $existingCartItem->save();
            } else {
                $cartItem = new CartItem();
                $cartItem->user_id = $userId;
                $cartItem->product_id = $product->id;
                $cartItem->quantity = 1;
                $cartItem->save();
            }

            return response()->json(['message' => 'Product added to cart successfully']);
        } else {
            return response()->json(['message' => 'Authentication required to add products to cart'], 401);
        }
    }

    public function removeFromCart(Request $request, Product $product)
    {
        $user = Auth::user();

        $cartItem = CartItem::where('user_id', $user->id)
                            ->where('product_id', $product->id)
                            ->first();

        if ($cartItem) {
            $cartItem->delete();
            return response()->json(['message' => 'Product removed from the cart']);
        }

        return response()->json(['message' => 'Product not found in the cart']);
    }

    public function checkout(Request $request)
    {
        $user = Auth::user();
        $cartItems = CartItem::where('user_id', $user->id)->get();
        
        $totalPrice = $cartItems->sum(function ($cartItem) {
            return $cartItem->quantity * $cartItem->product->price;
        });

        $shipStreet = $user->ship_street;
        $shipCity = $user->ship_city;
        $shipState = $user->ship_state;
        $shipZipCode = $user->ship_zip_code;

        if ($shippingAddress = $user->shippingAddresses()->first()) {
            $shipStreet = $shippingAddress->ship_street;
            $shipCity = $shippingAddress->ship_city;
            $shipState = $shippingAddress->ship_state;
            $shipZipCode = $shippingAddress->ship_zip_code;
    
            $shippingAddressString = $shipStreet . ', ' . $shipCity . ', ' . $shipState . ', ' . $shipZipCode;
    
        } else {
            return response()->json(['message' => 'Shipping address not found for the user'], 404);
        }

        $lastOrderId = Order::max('order_id'); 
        $nextOrderId = $lastOrderId + 1;

        foreach ($cartItems as $cartItem) {
            $order = new Order();
            $order->user_id = $user->id;
            $order->total_price = $totalPrice;
            $order->ship_details = $shippingAddressString;
            $order->id = $cartItem->order_id;
            $order->product_id = $cartItem->product_id;
            $order->product_qty = $cartItem->quantity;
            $order->order_id = $nextOrderId;
    
            $order->save();

            $product = Product::find($cartItem->product_id);
            if ($product) {
                $product->decrement('stock_qty', $cartItem->quantity);
                $product->save();
            }
        }

        $user->cartItems()->delete();

        return response()->json(['message' => 'Checkout successful. Order placed']);
    }
}
