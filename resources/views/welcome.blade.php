<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=, initial-scale=1.0">
    <title>Document</title>
</head>
<body style="font-family: Arial, sans-serif; background-color: #f2f2f2; margin: 0; padding: 0; align-items: center; justify-content: center; height: 100vh;">
    @auth
    <p>Congrats, you are logged in!</p>
    <form action="/users/logout" method="POST" onsubmit="return confirm('Are you sure you want to logout?');">
        @csrf
        <button type="submit" style="font-size: 14px; padding: 6px 10px; background-color: #333; color: #fff; border: none; border-radius: 4px; cursor: pointer">Logout</button>
    </form>
    <a href="/users/edit-profile" style="font-size: 14px; padding: 6px 10px; background-color: #333; color: #fff; border: none; border-radius: 4px; cursor: pointer; text-decoration: none;">Edit Info</a>
    <form action="/orders" method="GET">
        @csrf
        <button style="font-size: 14px; padding: 6px 10px; background-color: #333; color: #fff; border: none; border-radius: 4px; cursor: pointer">Order History</button>
    </form>
    <form action="/products/search" method="POST" style="margin-top: 20px;">
        @csrf
        <input name="search" type="text" placeholder="Search products..." style="font-size: 14px; padding: 6px; margin-bottom: 5px;">
        <button type="submit" style="font-size: 14px; padding: 6px 10px; background-color: #333; color: #fff; border: none; border-radius: 4px; cursor: pointer;">Search</button>
    </form>
    <a href="/cart/show" style="font-size: 14px; margin-top: 10px; text-decoration: none; color: #333;">Cart</a>
    <div style="border: 2px solid gray;">
        <h2 style="padding-left: 10px">Products</h2>
        @foreach($products as $product)
            <form action="/cart/add/{{$product->id}}" method="POST">
            @csrf 
            <div style="display: flex; align-items: center; background-color: gray; padding: 10px; margin: 10px;">
                <div style="flex: 1;">
                    <h3>{{$product['title']}}</h3>
                    <h3>{{$product['price']}}</h3>
                </div>
                <div style="display: flex; align-items: center;">
                <button type="submit" style="text-decoration: none; color: #333; margin-right: 10px; font-size: 14px; padding: 6px 10px; background-color: #333; color: #fff; border: none; border-radius: 4px; cursor: pointer;">Add</button>
                </div>
            </div>
            </form>
        @endforeach
    </div>
    @else
    <div style="position: absolute; top: 20px; left: 20px;">
        <div style="margin-bottom: 15px;">
        <h2 style="font-size: 20px; color: #333; margin-bottom: 5px;">Register</h2>
        <form action="/users/register" method="POST">
            @csrf
            <input name='name' type="text" placeholder="name" style="font-size: 14px; padding: 6px; margin-bottom: 5px;">
            <input name='email' type="text" placeholder="email" style="font-size: 14px; padding: 6px; margin-bottom: 5px;">
            <input name='password' type="password" placeholder="password" style="font-size: 14px; padding: 6px; margin-bottom: 5px;">
            <input name='ship_street' type="text" placeholder="ship_street" style="font-size: 14px; padding: 6px; margin-bottom: 5px;">
            <input name='ship_city' type="text" placeholder="ship_city" style="font-size: 14px; padding: 6px; margin-bottom: 5px;">
            <input name='ship_state' type="text" placeholder="ship_state" style="font-size: 14px; padding: 6px; margin-bottom: 5px;">
            <input name='ship_zip_code' type="text" placeholder="ship_zip_code" style="font-size: 14px; padding: 6px; margin-bottom: 5px;">
            <input name='phone' type="text" placeholder="phone" style="font-size: 14px; padding: 6px; margin-bottom: 5px;">
            <input name='bio' type="text" placeholder="bio" style="font-size: 14px; padding: 6px; margin-bottom: 5px;">
            <input name='weight' type="text" placeholder="weight" style="font-size: 14px; padding: 6px; margin-bottom: 5px;">
            <input name='height' type="text" placeholder="height" style="font-size: 14px; padding: 6px; margin-bottom: 5px;">
            <select name='gender' style="font-size: 16px; padding: 12px; margin-bottom: 10px;">
                <option value="M">Male</option>
                <option value="F">Female</option>
            </select>            
        <button style="font-size: 14px; padding: 6px 10px; background-color: #333; color: #fff; border: none; border-radius: 4px;">Register</button>
        </form>
    </div>
        <div>
        <h2 style="font-size: 20px; color: #333; margin-bottom: 5px;">Login</h2>
        <form action="/users/login" method="POST">
            @csrf
            <input name='loginName' type="text" placeholder="name" style="font-size: 14px; padding: 6px; margin-bottom: 5px;">
            <input name='loginPass' type="password" placeholder="password" style="font-size: 14px; padding: 6px; margin-bottom: 5px;">
            <button style="font-size: 14px; padding: 6px 10px; background-color: #333; color: #fff; border: none; border-radius: 4px;">Login</button>
        </form>
        </div>
    </div>  
    <div style="text-align: center; max-width: 800px; margin: 20px auto 0; padding-top: 40px;">
    <h2 style="font-size: 36px; color: #555; margin-bottom: 20px;">Products</h2>
        @if ($products->isEmpty())
            <div style="background-color: #ddd; padding: 30px; border-radius: 8px;">
                <h3 style="font-size: 28px; color: #333; margin: 0;">No products available</h3>
            </div>
        @else
            <div style="display: flex; flex-wrap: wrap; justify-content: space-between;">
                @foreach ($products as $product)
                    <div style="width: calc(25% - 20px); margin-bottom: 20px; background-color: #ddd; padding: 20px; border-radius: 8px; box-sizing: border-box;">
                        <h3 style="font-size: 20px; color: #333; margin-bottom: 10px;">{{ $product->title }}</h3>
                        <!-- Display other product details here -->
                    </div>
                @endforeach
            </div>
        @endif
    </div>
    @endauth
</body>
</html>