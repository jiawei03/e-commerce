<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <form action="/users/edit-profile" method="POST">
        @csrf
        @method('PUT')
        <input name='name' type="text" placeholder="Name" style="font-size: 14px; padding: 6px; margin-bottom: 5px;" value="{{ auth()->user()->name }}">
        <br>

        <input name='email' type="text" placeholder="Email" style="font-size: 14px; padding: 6px; margin-bottom: 5px;" value="{{ auth()->user()->email }}">
        <br>

        <input name='password' type="password" placeholder="New Password" style="font-size: 14px; padding: 6px; margin-bottom: 5px;">
        <br>  

        <input name='ship_street' type="text" placeholder="Street Address" style="font-size: 14px; padding: 6px; margin-bottom: 5px;" value="{{ auth()->user()->shippingAddresses()->exists() ? auth()->user()->shippingAddresses()->first()->ship_street : '' }}">
        <br>

        <input name='ship_city' type="text" placeholder="City" style="font-size: 14px; padding: 6px; margin-bottom: 5px;" value="{{ auth()->user()->shippingAddresses()->exists() ? auth()->user()->shippingAddresses()->first()->ship_city : '' }}">
        <br>

        <input name='ship_state' type="text" placeholder="State" style="font-size: 14px; padding: 6px; margin-bottom: 5px;" value="{{ auth()->user()->shippingAddresses()->exists() ? auth()->user()->shippingAddresses()->first()->ship_state : '' }}">
        <br>

        <input name='ship_zip_code' type="text" placeholder="Zip Code" style="font-size: 14px; padding: 6px; margin-bottom: 5px;" value="{{ auth()->user()->shippingAddresses()->exists() ? auth()->user()->shippingAddresses()->first()->ship_zip_code : '' }}">
        <br>

        <input name='phone' type="text" placeholder="Phone Number" style="font-size: 14px; padding: 6px; margin-bottom: 5px;" value="{{ auth()->user()->phone }}">
        <br>

        <input name='bio' type="text" placeholder="Bio" style="font-size: 14px; padding: 6px; margin-bottom: 5px;" value="{{ auth()->user()->bio }}">
        <br>  

        <input name='weight' type="text" placeholder="Weight" style="font-size: 14px; padding: 6px; margin-bottom: 5px;" value="{{ auth()->user()->weight }}">
        <br>

        <input name='height' type="text" placeholder="Height" style="font-size: 14px; padding: 6px; margin-bottom: 5px;" value="{{ auth()->user()->height }}">
        <br>  

        <select name='gender' style="font-size: 16px; padding: 8px; margin-bottom: 10px;">
            <option value="M" {{ (auth()->user()->gender === 'M') ? 'selected' : '' }}>Male</option>
            <option value="F" {{ (auth()->user()->gender === 'F') ? 'selected' : '' }}>Female</option>
        </select>

        <button type="submit" style="font-size: 14px; padding: 6px 10px; background-color: #333; color: #fff; border: none; border-radius: 4px;">Save Changes</button>
    </form>
</body>
</html>