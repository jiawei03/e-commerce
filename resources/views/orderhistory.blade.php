<!DOCTYPE html>
<html>
<head>
    <title>Order History</title>
</head>
<body>
    <h1>Order History</h1>

    @if(count($orders) > 0)
        @php
            $groupedOrders = $orders->groupBy('order_id');
        @endphp

        <ul>
            @foreach($groupedOrders as $orderId => $orderGroup)
                @php
                    $firstOrder = $orderGroup->first();
                @endphp
                <li>
                Order ID: {{ $orderId }} <br>
                <ul>
                    @foreach($orderGroup as $order)
                        @php
                            $product = App\Models\Product::find($order->product_id);
                        @endphp

                        @if ($product)
                            <li>
                                Product Name: {{ $product->title }} <br>
                                Quantity: {{ $order->product_qty }} <br>
                                Price: ${{ $product->price }} 
                            </li>
                        @endif
                    @endforeach
                    <br>
                    Total Price: ${{ $firstOrder->total_price }} <br>
                </ul>
            </li>
            <br>
            @endforeach
        </ul>
    @else
        <p>No orders found.</p>
    @endif
</body>
</html>