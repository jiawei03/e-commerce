<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\Product;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Product>
 */
class ProductFactory extends Factory
{
    protected $model = Product::class;

    public function definition()
    {
        return [
            'title' => $this->faker->text(10),
            'description' => $this->faker->text(15),
            'price' => $this->faker->randomFloat(2, 10, 1000),
            'stock_qty' => $this->faker->numberBetween(1, 200),
        ];
    }
}