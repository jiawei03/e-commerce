<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\CartController;
use App\Models\Cart;
use App\Models\Product;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

// Route::get('/', function () {
//     $products = Product::all();
//     return view('welcome', ['products' => $products]);
//     //return response()->json($products);
// });

// // Routes for Users
// Route::prefix('users')->group(function () {
//     Route::post('/register', [UserController::class, 'register']);
//     Route::post('/logout', [UserController::class, 'logout']);
//     Route::post('/login', [UserController::class, 'login']);
//     Route::get('/show', [UserController::class, 'index']); 
//     Route::get('/edit-profile', [UserController::class, 'showEditProfileForm']); 
//     Route::put('/edit-profile', [UserController::class, 'update']);
// });

// //Routes for cart
// Route::prefix('carts')->group(function () {
//     Route::get('/show', [CartController::class, 'index']); 
//     Route::post('/add/{product}', [CartController::class, 'addToCart']); 
//     Route::post('/remove/{product}', [CartController::class, 'removeFromCart']);
//     Route::get('/checkout', [CartController::class, 'checkout']);
// });

// Route::get('orders', [OrderController::class, 'index']);

// // Product Routes
// Route::prefix('products')->group(function () {
//     Route::get('/show', [ProductController::class, 'index']); 
//     Route::get('/{product}', [ProductController::class, 'productShow']);
//     Route::get('/category/{category}', [ProductController::class, 'productCategory']);
//     Route::post('/search', [ProductController::class, 'search']);
// });
